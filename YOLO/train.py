from ultralytics import YOLO
import torch


model = YOLO('yolov8n.pt')

results = model.train(
        data='D:/Katerina/yolo_signs/traffic-sign-recognition-full-annotated-v2/data.yaml',
        epochs=100,
        imgsz=640,
        device=torch.device("cuda")
    )
Были обучены модели YOLO (детектор+классификатор): yolov8n и yolov8s. Веса лучшей модели сохранены в файл best.pt. Ниже на рисунке видны графики обучения. Максимальный score, который был достигнут для yolov8n: mAP50 - 0.751; mAP50-95 - 0.561; precision - 0.731; f1-score - 0.697.

![Alt text](results_yolov8n.png)

Максимальный score, который был достигнут для yolov8n: mAP50 - 0.848; mAP50-95 - 0.643; precision - 0.766; f1-score - 0.776.

![Alt text](results_yolov8s.png)
Классификация происходит на 155 классах. Ниже виден пример работы модели.

![Alt text](example.png)

Данные для обучения:
https://disk.yandex.com/d/Ft655tfNMhC0QA

Веса обученных моделей:
https://disk.yandex.ru/d/cSyl68ck_BBonw

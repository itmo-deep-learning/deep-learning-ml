from ultralytics import YOLO


model = YOLO('yolov8n.pt')

results = model.train(
    data='./traffic-sign-recognition-1/data.yaml',
    epochs=100,
    imgsz=640,
    device='mps',
    single_cls=True,
)

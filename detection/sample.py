import os
import random
import shutil


yolo_anno_path = "./yolo_anno"
yolo_anno_10k_path = "./yolo_anno_10k"
rtsd_frames_path = "./rtsd-frames/rtsd-frames"
rtsd_frames_10k_path = "./rtsd-frames-10k"


os.makedirs(yolo_anno_10k_path, exist_ok=True)
os.makedirs(rtsd_frames_10k_path, exist_ok=True)

random.seed(0)


selected_files = random.sample(os.listdir(yolo_anno_path), 10_000)

for txt_file in selected_files:
    txt_file_path = os.path.join(yolo_anno_path, txt_file)
    shutil.copy(txt_file_path, yolo_anno_10k_path)


for txt_file in selected_files:
    jpg_file = os.path.join(rtsd_frames_path, txt_file.replace(".txt", ".jpg"))
    if os.path.exists(jpg_file):
        shutil.copy(jpg_file, rtsd_frames_10k_path)

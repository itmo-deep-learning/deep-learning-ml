import json

from pybboxes import BoundingBox
from tqdm import tqdm

with open('./train_anno.json') as f:
    train_anno = json.load(f)


imgid2data = {}
for img in train_anno['images']:
    imgid2data[img['id']] = {'width': img['width'], 'height': img['height'], 'file_name': img['file_name']}


yolo_anno = []
for anno in tqdm(train_anno['annotations'], total=len(train_anno['annotations'])):
    bbbox = anno['bbox']
    width = imgid2data[anno['image_id']]['width']
    height = imgid2data[anno['image_id']]['height']

    with open(f"./yolo_anno/{imgid2data[anno['image_id']]['file_name'].split('/')[1].split('.')[0]}.txt", 'w') as f:
        my_coco_box = bbbox
        coco_bbox = BoundingBox.from_coco(*my_coco_box)
        coco_bbox.image_size = (width, height)
        yolo_bbox = coco_bbox.to_yolo()
        a, b, c, d = yolo_bbox.values
        f.write(f'0 {a} {b} {c} {d} ')

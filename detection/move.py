import os
import random
import shutil

source_directory = "rtsd-frames"
destination_directory = "rtsd-frames-10k"
num_files_to_copy = 10_000

os.makedirs(destination_directory, exist_ok=True)

all_files = [f for f in os.listdir(source_directory) if os.path.isfile(os.path.join(source_directory, f))]

files_to_copy = random.sample(all_files, min(num_files_to_copy, len(all_files)))

for file_name in files_to_copy:
    source_path = os.path.join(source_directory, file_name)
    destination_path = os.path.join(destination_directory, file_name)
    shutil.copy2(source_path, destination_path)

import os
import shutil

folder1_path = "./yolo_anno"
folder2_path = "./rtsd-frames-10k"
destination_path = "./yolo_anno_10k"


for txt_file in os.listdir(folder1_path):
    if txt_file.endswith(".txt"):
        jpg_file = os.path.join(folder2_path, txt_file.replace(".txt", ".jpg"))
        if os.path.exists(jpg_file):
            print("yes")
            shutil.copy(os.path.join(folder1_path, txt_file), destination_path)
